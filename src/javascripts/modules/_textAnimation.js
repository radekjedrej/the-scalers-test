export class TextAnimation {
  constructor() {
    this.text = document.querySelector(".card-carousel__heading")

    this.init()
  }

  init() {
    const strText = this.text.textContent
    const splitText = strText.split("")
    this.text.textContent = ""

    for (let i = 0; i < splitText.length; i++) {
      this.text.innerHTML += `<span> ${splitText[i]} </span>`
    }

    let char = 0
    let timer = setInterval(() => {
      let span = this.text.querySelectorAll("span")[char]
      span.classList.add("card-carousel__heading--fade")
      char++
      if (char === splitText.length) {
        clearInterval(timer)
        timer = null
        return
      }
    }, 50)
  }
}
