import Swiper from "swiper"

export class Slider {
  constructor() {
    this.cards

    this.init()
  }

  init() {
    this.cardsSlider()
  }

  cardsSlider() {
    this.cards = new Swiper(".card-carousel__container", {
      resistanceRatio: 0,
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }
    })
  }
}
