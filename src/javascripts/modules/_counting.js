export class Counting {
  constructor() {
    this.counters = document.querySelectorAll(".info__number__count")
    this.speed = 100 // The lower the slower

    this.init()
  }

  init() {
    this.counters.forEach(counter => {
      // start with 0 by default
      counter.innerText = "0"

      const updateCounter = () => {
        const target = +counter.getAttribute("data-target")
        const c = +counter.innerText

        // get the 0.1% to speed up things
        const increment = target / 1000

        if (c < target) {
          counter.innerText = `${Math.ceil(c + increment)}`
          setTimeout(updateCounter, 1)
        } else {
          counter.innerText = target
        }
      }

      updateCounter()
    })
  }
}
