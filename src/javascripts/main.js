import { handleWindow } from "./modules/_helpers"
import { Slider } from "./modules/_carousels"
import { Counting } from "./modules/_counting"
import { TextAnimation } from "./modules/_textAnimation"

window.addEventListener("DOMContentLoaded", () => {
  // Variables
  const swiperContainer = document.querySelector(".swiper-container")
  const countNumber = document.querySelector(".info__number__count")
  const letterContainer = document.querySelector(".card-carousel__heading")
  // Object Instance
  if (swiperContainer) {
    new Slider()
  }

  if (countNumber) {
    new Counting()
  }

  if (letterContainer) {
    new TextAnimation()
  }

  handleWindow()
})
